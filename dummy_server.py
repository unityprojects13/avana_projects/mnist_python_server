from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
from PIL import Image
import io
from torchvision import datasets,transforms

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

class exampleCNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)  # first convolutional layer
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5) # second convolutional layer
        self.fc1 = nn.Linear(20*4*4, 10) # fully connected layer

    def forward(self, x):
        print("Original input size:\t {}".format(x.shape))
        x = self.conv1(x)
        print("Size after conv1:\t {}".format(x.shape))
        x = F.max_pool2d(x, 2)
        print("Size after max pool 1:\t {}".format(x.shape))
        x = F.relu(x)
        x = self.conv2(x)
        print("Size after conv2:\t {}".format(x.shape))
        x = F.max_pool2d(x, 2)
        print("Size after max pool 2:\t {}".format(x.shape))
        x = F.relu(x)
        x = x.view(-1, 16*20)
        print("Size before fc1:\t {}".format(x.shape))
        x = self.fc1(x)
        print("Size after fc1:\t\t {}".format(x.shape))
        return F.log_softmax(x, dim=1)   

model = exampleCNN()


app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"


@app.route("/", methods=["POST"])
@cross_origin()
def event():
    try:
        contents=request.files.get("bytes_image").read()
        image=Image.frombytes(data=contents,size=(28,28),mode="L").transpose(Image.Transpose.FLIP_TOP_BOTTOM)
        # image.show()
        # image=Image.open(io.BytesIO(contents))
    except Exception as e:
        print(e)

    transform=transforms.Compose([transforms.PILToTensor()])
    tensor=transform(image)
    img_tensor=((tensor-0.1307)/0.3081).view(1,28,28)
    output=model(img_tensor)
    new, predicted = torch.max(output.data, 1)
    prediction=predicted.to(torch.device("cpu")).item()
    print("result:"+str(prediction))
    return jsonify({"result":prediction})

  

if __name__=="__main__":
    model.load_state_dict(torch.load("./cnn_model1.model",map_location=torch.device('cpu')))
    app.run(host="0.0.0.0",port="5000")