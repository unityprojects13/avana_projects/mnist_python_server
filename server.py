import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class exampleCNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)  # first convolutional layer
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5) # second convolutional layer
        self.fc1 = nn.Linear(20*4*4, 10) # fully connected layer

    def forward(self, x):
        print("Original input size:\t {}".format(x.shape))
        x = self.conv1(x)
        print("Size after conv1:\t {}".format(x.shape))
        x = F.max_pool2d(x, 2)
        print("Size after max pool 1:\t {}".format(x.shape))
        x = F.relu(x)
        x = self.conv2(x)
        print("Size after conv2:\t {}".format(x.shape))
        x = F.max_pool2d(x, 2)
        print("Size after max pool 2:\t {}".format(x.shape))
        x = F.relu(x)
        x = x.view(-1, 16*20)
        print("Size before fc1:\t {}".format(x.shape))
        x = self.fc1(x)
        print("Size after fc1:\t\t {}".format(x.shape))
        return F.log_softmax(x, dim=1)   


model = exampleCNN()
model.load_state_dict(torch.load("./cnn_model1.model",map_location=torch.device('cpu')))


## code to receive data
## check if image is 28*28


image=None

t1=((image-0.1307)/0.3081).view(1,28,28)
output=model(t1)
new, predicted = torch.max(output.data, 1)
predicted.to(torch.device("cpu")).item()

## return response